from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import os
from dotenv import load_dotenv
from sqlalchemy.orm import declarative_base
load_dotenv()


try:    
    connect = os.environ.get('DB_URI', 'test')
    engine = create_engine(connect)
    Session = scoped_session(sessionmaker(bind=engine))
    session = Session()
    Base = declarative_base()

except Exception as e:
    raise Exception(e)