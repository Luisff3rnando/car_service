from django.urls import path

from car_service.views.views import ScheduleView
from car_service.views.views import ServiceView

urlpatterns = [
    path(
        route='schedule',
        view=ScheduleView.as_view(),
        name='schedule'
    ),
    path(
        route='service',
        view=ServiceView.as_view(),
        name='service'
    )
]
