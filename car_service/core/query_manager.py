from abc import ABC
from abc import abstractmethod
from sentry_sdk import capture_exception
from sqlalchemy import desc
from sqlalchemy_paginator import Paginator
from .exceptions import FileNotFound
from .exceptions import GeneralException

from config.conexion import session


class Mixin(ABC):
    """
    """

    @abstractmethod
    def get(self):
        pass

    @abstractmethod
    def get_all(self):
        pass

    @abstractmethod
    def create(self):
        pass

    @abstractmethod
    def filter(self):
        pass

    @abstractmethod
    def update(self):
        pass

    @abstractmethod
    def delete(self):
        pass


class GenericQuerySet(Mixin):
    """
    """

    def __init__(self, entity):
        self.entity = entity

    def get(self,
            data,
            page,
            per_page,
            message=None):
        """
        Get one data include filters, order_by, columns to orders
        """
        try:
            _instance = session.query(self.entity).filter_by(
                **data).order_by()

            _paginator = Paginator(_instance, per_page)
            _page = _paginator.page(page)

            meta = {
                "data": "",
                "current_page": str(_page),
                "item_per_page": per_page,
                "total_page": _page.paginator.total_pages,
                "total_items": len(_page.object_list),
                "available_orders": [
                    "ASC",
                    "DESC"
                ]
            }
            return meta, _page.object_list,

        except Exception as e:
            session.rollback()
            capture_exception(Exception)
            raise FileNotFound(e)

    def get_all(self, kwargs=None):
        try:
            instance = session.query(self.entity).all()
            return instance
        except Exception as e:
            raise Exception(e)

    def filter(self, kwargs):
        try:
            _instance = session.query(self.entity).filter_by(
                **kwargs)
            return _instance

        except Exception as e:
            session.rollback()
            capture_exception(Exception)
            raise Exception("{} not found".format(e))

        finally:
            session.close()
            raise Exception("{} not found".format(e))

    def delete(self, kwargs):
        try:
            _instance = session.query(self.entity).filter_by(
                **kwargs).delete()
            return _instance

        except Exception as e:
            session.rollback()
            capture_exception(Exception)
            raise Exception("{} not found".format(e))

        finally:
            session.close()
            raise Exception("{} not found".format(e))

    def create(self, kwargs):
        try:
            _instance = self.entity(**kwargs)
            session.add(_instance)
            session.commit()
            return _instance

        except Exception as e:
            session.rollback()
            capture_exception(Exception)
            raise GeneralException(e)
        finally:
            session.close()

    def update(self, kwargs):
        try:
            instance = self.entity.objects.filter(**kwargs)
            instance.update()
            return instance
        except Exception as e:
            raise Exception(e)
