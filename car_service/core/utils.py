
"""
"""
import ast
import requests
from sentry_sdk import capture_exception


def http_request(url, params: dict = None):
    """
    Generic Http request
    """
    try:
        headers = {
            'Content-Type': 'application/json',
        }

        response = requests.get(
            url,
            params=params,
            headers=headers
        )
        response = response.content.decode('utf-8')
        response = ast.literal_eval(response)
        return response
    except Exception as e:
        capture_exception(e)
        raise Exception(str(e))
