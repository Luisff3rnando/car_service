"""
"""
import time
# models
from car_service.models.service import Service
from car_service.models.service import ServiceType
from car_service.models.worker import Worker
from car_service.models.user import Schedule
# queryset
from car_service.core.query_manager import GenericQuerySet
# utils
from .utils import http_request
from car_service.core.exceptions import GeneralException
# serializers
from car_service.serializers.services import ScheduleModelSerializers
from car_service.serializers.services import ServicesModelSerializers

# constantes
from car_service.core.constants import STATE_SERVICE
from car_service.core.constants import HOST_MECHANICAL_WORKSHOP


class Workers:
    """
    - Crear worker
    - List Worker
    - List services Worker
    - Delete Worker
    """

    def create_worker(self, kwargs):
        pass

    def get_all_worker(self, kwargs):
        pass

    def get_one_worker(self, kwargs):
        pass

    def get_one_services(self, kwargs):
        pass


# class ServiceHaversine:

#     def process_haversine(self, services, data):
#         """Procesar metodo haversine"""
#         if ('latitude' in data) and ('longitude' in data):
#             data = list(map(lambda x: self.get_service(x, data), services))
#             data = list(filter(lambda x: x, data))
#             return data
#         else:
#             return services

#     def get_service(self, service, data):
#         """Obtener Servicios"""
#         destine_latitude = Decimal(service['stops'][0]['latitude'])
#         destine_longitude = Decimal(service['stops'][0]['longitude'])
#         origin_longitude = Decimal(data.get('longitude'))
#         origin_latitude = Decimal(data.get('latitude'))

#         service_radius = haversine(
#             (destine_latitude, destine_longitude),
#             (origin_latitude, origin_longitude)
#         )

#         if service_radius <= 5:
#             return service
#         else:
#             return False

class Services:
    """
    - Create Services
    - Get Services
    - Update Services
    - Cancel Services
    - List Services
    - Create schedule
    """

    def create_services(self, kwargs):
        """
        """
        try:
            # consultar talleres
            _mechanical_workshop = http_request(HOST_MECHANICAL_WORKSHOP)
            mechanical_workshop = {
                "mechanical_workshop_id": _mechanical_workshop.get('id'),
                "mechanical_workshop_name":  _mechanical_workshop.get('name'),
                "destination_latitude": _mechanical_workshop['location']['lat'],
                "destination_longitude": _mechanical_workshop['location']['lng'],
                "destination_address": _mechanical_workshop['address'],
            }
            kwargs = dict(**kwargs, **mechanical_workshop)

            # iniciar servicio con estado asignado
            kwargs['service_state_id'] = STATE_SERVICE['ASSIGNED']

            # save services
            _service = GenericQuerySet(Service).create(kwargs)
            return _service
        except Exception as e:
            raise GeneralException(e)

    def update_services(self, kwargs):
        pass

    def cancel_services(self, kwargs):
        pass

    def get_services(self, kwargs):
        """
        Get one and filter
        """
        page = kwargs.get('page')
        kwargs.pop('page')
        per_page = kwargs.get('per_page')
        kwargs.pop('per_page')

        # filter all retorna los servicios activos e inactivos
        if kwargs['all']:
            kwargs.pop('active')

        kwargs.pop('all')

        paginator, data = GenericQuerySet(Service).get(data=kwargs,
                                                       page=page,
                                                       per_page=per_page)

        response = ServicesModelSerializers(data, many=True).data
        paginator['data'] = response
        return paginator

    def filter(self):
        pass


class Schedules:
    """
    """

    def create_schedule(self, kwargs):
        """
        """
        try:
            _schedule = GenericQuerySet(Schedule).create(kwargs)
            result = ScheduleModelSerializers(_schedule).data
            return result
        except Exception as e:
            raise GeneralException(e)

    def get_all_schedule(self, kwargs=None):
        try:
            _schedule = GenericQuerySet(Schedule).get_all()
            result = ScheduleModelSerializers(_schedule, many=True).data
            return result
        except Exception as e:
            raise GeneralException(e)

    def get_one_schedule(self, kwargs):
        pass

    def get_one_schedule(self, kwargs):
        pass

    def delete_schedule(self, kwargs):
        try:
            _schedule = GenericQuerySet(Schedule).delete(kwargs)
            result = ScheduleModelSerializers(_schedule, many=True).data
            return result
        except Exception as e:
            raise GeneralException(e)
