"""
"""
from car_service.core.process import Schedules
from car_service.core.process import Services
from car_service.core.process import Workers


class ScheduleHandler:
    """
    """

    @staticmethod
    def create(kwargs):
        return Schedules().create_schedule(kwargs)

    @staticmethod
    def get_one(kwargs):
        return Schedules().get_one_schedule(kwargs)

    @staticmethod
    def get_all():
        return Schedules().get_all_schedule()

    @staticmethod
    def delete(kwargs):
        return Schedules().delete_schedule(kwargs)


class ServicesHandler:
    """
    """
    @staticmethod
    def create(kwargs):
        return Services().create_services(kwargs)

    @staticmethod
    def get_one(kwargs):
        return Services().get_services(kwargs)

    @staticmethod
    def get_all():
        return Services().get_all_services()

    # @staticmethod
    # def delete(kwargs):
    #     return Services().delete_schedule(kwargs)


class WorkerHandler:
    """
    """

    @staticmethod
    def create(kwargs):
        pass
