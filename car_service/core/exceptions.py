class GeneralException(Exception):
    '''Excepcion propia.'''
    code_error = 1001
    message = ""


class FileNotFound(Exception):
    '''Excepcion propia.'''
    code_error = 1002
    message = "File not found"
