from uuid import uuid4
from sqlalchemy import Column
from sqlalchemy import Text
from sqlalchemy import JSON
from sqlalchemy import Integer
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql.elements import Null


# base model
from .base_model import BaseModel


def generate_uuid():
    return str(uuid4())


class ServiceType(BaseModel):
    """
    """
    __tablename__ = 'service_type'
    id = Column(Integer, primary_key=True)
    uuid = Column(Text, default=generate_uuid)
    name = Column(Text)
    description = Column(Text)
    
    service = relationship( 'Service', backref='service_type')

    def __repr__(self):
        return "<ServiceType(uuid='%s', name='%s')>" % (
            self.uuid, self.name)


class ServiceState(BaseModel):
    """
    """
    __tablename__ = 'service_state'
    id = Column(Integer, primary_key=True)
    uuid = Column(Text, default=generate_uuid)
    name = Column(Text)
    description = Column(Text)
    
    
    service = relationship( 'Service', backref='service_state')

    def __repr__(self):
        return "<ServiceState(uuid='%s', name='%s')>" % (
            self.uuid, self.name)


class Service(BaseModel):
    """
    """
    __tablename__ = 'services'
    id = Column(Integer, primary_key=True)
    uuid = Column(Text, default=generate_uuid)
    # refactor tiposde dato hora y fecha, relaciones
    date = Column(Text)
    # tiposd edatos float
    origin_latitude = Column(Text)
    origin_longitude = Column(Text)
    origin_address = Column(Text)

    destination_longitude = Column(Text)
    destination_latitude = Column(Text)
    destination_address = Column(Text)

    service_state_id = Column(Integer, ForeignKey('service_state.id'))
    service_type_id = Column(Integer, ForeignKey('service_type.id'))
    worker_id = Column(Integer, ForeignKey('worker.id'))
    schedule_id = Column(Integer, ForeignKey('schedule.id'))
    user_id = Column(Integer)
    mechanical_workshop_id = Column(Integer)
    mechanical_workshop_name = Column(Text)
    additional_data = Column(JSON, default=Null)

    def __repr__(self):
        return "<Service(uuid='%s', service_type_id='%s', worker_id='%s')>" % (
            self.uuid, self.service_type_id, self.worker_id)
