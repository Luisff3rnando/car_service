# from django.contrib.auth.base_user import AbstractBaseUser
from uuid import uuid4
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy.orm import relationship

# base model
from .base_model import BaseModel


def generate_uuid():
    """
    """
    return str(uuid4())


# class User(BaseModel):
#     """
#     """
#     __tablename__ = 'users'
#     id = Column(Integer, primary_key=True)
#     uuid = Column(Text, default=generate_uuid)
#     first_name = Column(Text)
#     last_name = Column(Text)
#     nid = Column(String(128))
#     phone = Column(String(128))
#     address = Column(String(128))
#     profile = Column(String(128))

#     def __repr__(self):
#         return "<File(uuid='%s', nid='%s', phone='%s')>" % (
#             self.uuid, self.nid, self.phone)

# class Profile(BaseModel):
#     """
#     """
#     __tablename__ = 'profiles'
#     id = Column(Integer, primary_key=True)
#     name=Column(Text)
#     description=Column(Text)

class Schedule(BaseModel):
    __tablename__ = 'schedule'
    id = Column(Integer, primary_key=True)
    uuid = Column(Text, default=generate_uuid)
    name = Column(Text)
    start_hour = Column(Text)
    end_hour = Column(Text)
    
    service = relationship( 'Service', backref='schedule')

    def __repr__(self):
        return "<Schedule(uuid='%s',id='%s')>" % (
            self.uuid, self.id)
