from uuid import uuid4
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy.orm import relationship


# base model
from .base_model import BaseModel


def generate_uuid():
    return str(uuid4())


class Worker(BaseModel):
    """
    """
    __tablename__ = 'worker'

    id = Column(Integer, primary_key=True)
    uuid = Column(Text, default=generate_uuid)
    first_name = Column(Text)
    last_name = Column(Text)
    nid = Column(String(128))
    phone = Column(String(128))
    address = Column(String(128))

    service = relationship('Service', backref='worker')

    def __repr__(self):
        return "<Worker(uuid='%s')>" % (
            self.uuid)

    @property
    def full_name(self):
        _full_name = "{} {}".format(self.first_name, self.last_name)
        return _full_name
