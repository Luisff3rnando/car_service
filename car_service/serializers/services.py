from rest_framework import serializers


class ResponseSerializers(serializers.Serializer):
    message = serializers.CharField()
    data = serializers.CharField()
    code = serializers.CharField()


class ServiceSerializers(serializers.Serializer):
    """
    """
    service_type_id = serializers.IntegerField()
    worker_id = serializers.IntegerField()
    schedule_id = serializers.IntegerField()
    user_id = serializers.IntegerField()
    additional_data = serializers.DictField(required=False)
    date = serializers.CharField()


class FilterServiceSerializers(serializers.Serializer):
    """
    """
    page = serializers.IntegerField()
    per_page = serializers.IntegerField()
    id = serializers.CharField(required=False)
    uuid = serializers.CharField(required=False)
    worker_id = serializers.IntegerField(required=False)
    user_id = serializers.IntegerField(required=False)
    service_type_id = serializers.IntegerField(required=False)
    date = serializers.CharField(required=False)
    all = serializers.BooleanField(required=False)
    active = serializers.BooleanField(required=False, default=True)


class WorkerListSerializers(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    nid = serializers.CharField()


class ServicesModelSerializers(serializers.Serializer):
    id = serializers.CharField()
    active = serializers.BooleanField()
    uuid = serializers.CharField()
    date = serializers.CharField()
    additional_data = serializers.JSONField()

    origin = serializers.SerializerMethodField()
    destination = serializers.SerializerMethodField()

    mechanical_workshop = serializers.SerializerMethodField()
    service_type = serializers.SerializerMethodField()
    service_state = serializers.SerializerMethodField()
    # date = serializers.SerializerMethodField()
    user_id = serializers.IntegerField()
    worker = serializers.SerializerMethodField()

    def get_origin(self, queryset):
        origin = {
            "latitude": queryset.origin_latitude,
            "longitude": queryset.origin_longitude,
            "address": queryset.origin_address,
        }

        return origin

    def get_destination(self, queryset):
        origin = {
            "latitude": queryset.destination_latitude,
            "longitude": queryset.destination_longitude,
            "address": queryset.destination_address,
        }

        return origin

    def get_mechanical_workshop(self, queryset):
        mechanical_workshop = {
            "id":queryset.mechanical_workshop_id,
            "name":queryset.mechanical_workshop_name,
        }

        return mechanical_workshop

    def get_worker(self, queryset):
        worker = queryset.worker
        _worker = {
            "id": worker.id,
            "name": worker.full_name,
            "nid": worker.nid,
            "phone": worker.phone
        }
        return _worker

    def get_service_type(self, queryset):
        return queryset.service_type.name

    def get_service_state(self, queryset):
        return queryset.service_state.name

    def get_schedule(self, queryset):
        return queryset.schedule.name


class ScheduleSerializers(serializers.Serializer):
    """
    """
    name = serializers.CharField(required=False)
    start_hour = serializers.CharField()
    end_hour = serializers.CharField()


class ScheduleModelSerializers(serializers.Serializer):
    """
    """
    uuid = serializers.CharField()
    name = serializers.CharField(required=False)
    start_hour = serializers.IntegerField()
    hour_init = serializers.CharField()
    end_hour = serializers.CharField()
    active = serializers.BooleanField()
