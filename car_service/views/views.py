from drf_yasg import openapi
from drf_yasg.openapi import Schema
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

# serializers
from car_service.serializers.services import ResponseSerializers
from car_service.serializers.services import ServiceSerializers
from car_service.serializers.services import ScheduleSerializers
from car_service.serializers.services import ScheduleModelSerializers
from car_service.serializers.services import FilterServiceSerializers

# # handlers
from car_service.core.handler import ScheduleHandler
from car_service.core.handler import ServicesHandler


class ServiceView(APIView):
    @swagger_auto_schema(
        responses=None,
        request_body=ServiceSerializers,
        tags=['Services']
    )
    def post(self, request):
        """
        Create Service
        """

        try:
            serializer = ServiceSerializers(data=request.data)
            if not serializer.is_valid():
                return Response(
                    dict(success=False,
                         code=400,
                         message="Not valid request: " +
                         str(serializer.errors),
                         status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                )
            ServicesHandler.create(serializer.initial_data)
            return Response(
                dict(message="OK", data="OK", code=201),
                status=status.HTTP_201_CREATED
            )
        except Exception as e:
            return Response(
                dict(success=False,
                     code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                     message=str(e)),
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )

    @swagger_auto_schema(
        responses=None,
        request_body=None,
        operation_description="Get List Documents",
        tags=['Services'],
        query_serializer=FilterServiceSerializers
    )
    def get(self, request, format=None):
        """        
        parameters:
        -  nid
        -  page=1
        -  per_page=3
        -  field_order=nid
        """
        try:
            serializer = FilterServiceSerializers(data=request.query_params)
            if not serializer.is_valid():
                return Response(
                    dict(success=False,
                         code=400,
                         message="Not valid request: " +
                         str(serializer.errors),
                         status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                )
            handler = ServicesHandler.get_one(serializer.data)
            return Response(handler,
                            # dict(success=False,
                            #      code=0,
                            #      message="OK",
                            #      data=handler
                            #      ),
                            status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                dict(success=False,
                     code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                     message=str(e)),
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )


class ScheduleView(APIView):
    @swagger_auto_schema(
        responses={201: ResponseSerializers()},
        request_body=ScheduleSerializers,
        tags=['Schedule']
    )
    def post(self, request):
        """
        Create Schedule
        """

        try:
            serializer = ScheduleSerializers(data=request.data)
            if not serializer.is_valid():
                return Response(
                    dict(success=False,
                         code=400,
                         message="Not valid request: " +
                         str(serializer.errors),
                         status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                )
            handler = ScheduleHandler.create(serializer.initial_data)
            return Response(
                dict(message="OK", code=201, data=handler),
                status=status.HTTP_201_CREATED
            )
        except Exception as e:
            return Response(
                dict(success=False,
                     code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                     message=str(e)),
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )

    @swagger_auto_schema(
        responses={200: ScheduleModelSerializers()},
        request_body=None,
        operation_description="Get All Schedules",
        tags=['Schedule'],
    )
    def get(self, request, format=None):
        """
        """
        try:
            handler = ScheduleHandler.get_all()
            return Response(
                dict(success=False,
                     code=0,
                     message="OK",
                     data=handler
                     ),
                status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                dict(success=False,
                     code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                     message=str(e)),
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )

    @swagger_auto_schema(
        responses={200: ScheduleModelSerializers()},
        request_body=None,
        operation_description="Delete Schedules",
        tags=['Schedule'],
    )
    def delete(self, request, format=None):
        """
        """
        try:
            handler = ScheduleHandler.delete()
            return Response(
                dict(success=False,
                     code=0,
                     message="OK",
                     data=handler
                     ),
                status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                dict(success=False,
                     code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                     message=str(e)),
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )


class HealthView(APIView):
    @swagger_auto_schema(
        operation_description="Verify status of service",
        tags=["health"],
        operation_summary="API health check service",
        responses={
            200: 'Service is Ok'
        }
    )
    def get(self, request):
        return Response(
            dict(status="ok")
        )
